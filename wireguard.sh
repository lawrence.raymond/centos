#!/bin/bash

## Проверяем запущен ли скрипт от sudo
if [ "${EUID}" -ne 0 ]; then
	echo -e "\e[1;31mПожалуйста, запустите скрипт с помощью sudo!\e[0m"
	exit
fi

## Очищаем консоль
clear

## Приветствие и информация
echo -e "\n\e[1;33mДобро пожаловать в установщик \e[1;31mWireGuard\e[0;33m!\e[0m"
echo -e "\e[1;33mЧтобы продолжить установку, пожалуйста, ответьте на следующие вопросы.\e[0m\n"

## Собираем информацию, необходимую для установки
# Узнаем IP адрес сервера
WAN_IP=$(ip -4 addr | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p' | awk '{print $1}' | head -1)
read -rp "Введите IP адрес Вашего сервера: " -e -i "${WAN_IP}" WAN_IP

# Узнаем имя сетевого интерфейса сервера
WAN=$(ip -4 route ls | grep default | grep -Po '(?<=dev )(\S+)' | head -1)
read -rp "Введите название сетевого интерфейса Вашего сервера: " -e -i "${WAN}" WAN

# Узнаем порт для WireGuard сервера
until [[ ${WG_PORT} =~ ^[0-9]+$ ]] && [ "${WG_PORT}" -ge 1 ] && [ "${WG_PORT}" -le 65535 ]; do
read -rp "Введите порт для WireGuard сервера [1-65535]: " -e -i "51820" WG_PORT
done

## Устанавливаем репозиторий EPEL
echo -e "\e[1;33mУстановка репозитория EPEL...\e[0m"
sudo yum -y install epel-release elrepo-release yum-plugin-elrepo

## Устанавливаем WireGuard и необходимые элементы
echo -e "\e[1;33mУстановка WireGuard и необходимых элементов...\e[0m"
sudo yum -y install kmod-wireguard wireguard-tools iptables

## Устанавливаем WireGuard и необходимые элементы
echo -e "\e[1;33mСоздание директории для WireGuard'a...\e[0m"
sudo mkdir -p /etc/wireguard/
chmod 600 -R /etc/wireguard/

## Генерируем приватный и публичный ключи сервера
echo -e "\e[1;33mГенерация приватного и публичного ключей сервера...\e[0m"
SERVER_PRIVATE_KEY=$(wg genkey)
SERVER_PUBLIC_KEY=$(echo "${SERVER_PRIVATE_KEY}" | wg pubkey)

## Настраиваем сетевой интерфейс Вашего WireGuard сервера
echo -e "\e[1;33mНастройка сетевого интерфейса Вашего WireGuard сервера...\e[0m"
echo "[Interface]
Address = 10.66.66.1/24,fd42:42:42::1/64
ListenPort = ${WG_PORT}
PrivateKey = ${SERVER_PRIVATE_KEY}
PostUp = iptables -A FORWARD -i ${WAN} -o wg0 -j ACCEPT; iptables -A FORWARD -i wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o ${WAN} -j MASQUERADE; ip6tables -A FORWARD -i wg0 -j ACCEPT; ip6tables -t nat -A POSTROUTING -o ${WAN} -j MASQUERADE
PostDown = iptables -D FORWARD -i ${WAN} -o wg0 -j ACCEPT; iptables -D FORWARD -i wg0 -j ACCEPT; iptables -t nat -D POSTROUTING -o ${WAN} -j MASQUERADE; ip6tables -D FORWARD -i wg0 -j ACCEPT; ip6tables -t nat -D POSTROUTING -o ${WAN} -j MASQUERADE" >>"/etc/wireguard/wg0.conf"

## Включаем маршрутизацию на Вашем сервере
echo -e "\e[1;33mВключаем маршрутизацию на Вашем сервере...\e[0m"
echo "net.ipv4.ip_forward = 1
net.ipv6.conf.all.forwarding = 1" >/etc/sysctl.d/wg.conf
sysctl --system

## Запускаем WireGuard
echo -e "\e[1;33mЗапуск WireGuard...\e[0m"
systemctl start "wg-quick@wg0"
systemctl enable "wg-quick@wg0"

## Генерируем приватный и публичный ключи клиента
echo -e "\e[1;33mГенерация приватного и публичного ключей клиента...\e[0m"
cd /etc/wireguard
CLIENT_PRIVATE_KEY=$(wg genkey)
CLIENT_PUBLIC_KEY=$(echo "${CLIENT_PRIVATE_KEY}" | wg pubkey)
CLIENT_PRE_SHARED_KEY=$(wg genpsk)

## Настраиваем конфигурацию WireGuard клиента
echo -e "\e[1;33mНастройка конфигурации WireGuard клиента...\e[0m"
echo "[Interface]
PrivateKey = ${CLIENT_PRIVATE_KEY}
Address = 10.66.66.2/32,fd42:42:42::2/128
DNS = 94.140.14.14,94.140.15.15

[Peer]
PublicKey = ${SERVER_PUBLIC_KEY}
PresharedKey = ${CLIENT_PRE_SHARED_KEY}
Endpoint = ${WAN_IP}:${WG_PORT}
AllowedIPs = 0.0.0.0/0,::/0" >>"/etc/wireguard/client-wg0.conf"

## Настраиваем сетевой интерфейс Вашего WireGuard сервера
echo -e "\e[1;33mНастройка сетевого интерфейса Вашего WireGuard сервера...\e[0m"
echo "
[Peer]
PublicKey = ${CLIENT_PUBLIC_KEY}
PresharedKey = ${CLIENT_PRE_SHARED_KEY}
AllowedIPs = 10.66.66.2/32,fd42:42:42::2/128" >> "/etc/wireguard/wg0.conf"
systemctl restart "wg-quick@wg0"

## Просматриваем конфигурацию WireGuard клиента
clear
echo -e "\e[1;33mСодержимое конфигурационного client-wg0.conf файла для подключения:\n\e[0m"
cat /etc/wireguard/client-wg0.conf
echo -e " "
